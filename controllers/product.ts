import { ExoApi } from "../apis/exo-api";
import { HubspotApi } from "../apis/hubspot-api";
import { CreateHubspotProduct, ExoStockItem } from "../models/product";

export async function updateProducts(): Promise<void> {
	// Get the APIs
	const exoApi = new ExoApi();
	const hubspotApi = new HubspotApi();

	// Initialise the loop variables
	let fetchingExoStockItems: boolean = true;
	let exoStockItems: ExoStockItem[] = [];
	let exoStockItemPage: number = 1;
	let exoRes: ExoStockItem[];

	while (fetchingExoStockItems) {
		exoRes = await exoApi.getStockItems(exoStockItemPage);

		if (exoRes.length) {
			exoStockItems = [...exoStockItems, ...exoRes];
			console.log(
				`• [MYOB EXO]: fetched ${exoStockItems.length} stock item(s)...`
			);
			exoStockItemPage = exoStockItemPage += 1;
			if (exoRes.length != 100) {
				fetchingExoStockItems = false;
			}
		} else {
			fetchingExoStockItems = false;
		}
	}

	// Confirm Contacts
	console.log(`✓ [EXOSpot]: saved ${exoStockItems.length} stock item(s)`);

	let hubspotProducts: CreateHubspotProduct[] = exoStockItems.map(
		(stockItem) => {
			return {
				properties: {
					description: stockItem.description,
					hs_cost_of_goods_sold: String(stockItem.latestcost),
					hs_sku: stockItem.id,
					name: stockItem.id,
					price: String(
						stockItem.saleprices.find(
							(salePrice) => salePrice.name === "Base Price"
						)?.price
					),
				},
			};
		}
	);

	hubspotProducts.forEach(async (hubspotProduct) => {
		try {
			console.log(`• [MYOB EXO]: Creating product "${hubspotProduct.properties.description}"...`)
			await hubspotApi.createProduct(hubspotProduct)
		} catch (error) {
			// console.error(error);
		}
	});
}