const ENV = process.env.NODE_ENV || "development";
if (ENV !== "production") {
	require('dotenv').config();
}

import express from "express";
import { CronJob } from "cron";
import { updateProducts } from "./controllers/product";

const app = express();
const PORT = 8080;
app.get("/", (req, res) => res.send("MYOB EXO + HubSpot Integration"));
app.listen(PORT, async (): Promise<void> => {
	console.log(`⚡️[EXOSpot]: Server is running at https://localhost:${PORT}`);

	let cronJob: CronJob = new CronJob("0 0 6-17 * * *", async () => {
		try {
			await updateProducts();
		} catch (e) {
			console.error(e);
		}
	});

	// Start job
	if (!cronJob.running) {
		cronJob.start();
	}
});
