export interface ExoContact {
	jobtitle: string | null;
	firstname: string | null;
	lastname: string | null;
	fullname: string | null;
	directphonenumber: string | null;
	mobilephonenumber: string | null;
	email: string | null;
	postaladdress: {
		line1: string | null;
		line2: string | null;
		line3: string | null;
		line4: string | null;
		line5: string | null;
	};
	postalcode: string | null;
	deliveryaddress: {
		line1: string | null;
		line2: string | null;
		line3: string | null;
		line4: string | null;
		line5: string | null;
		line6: string | null;
	};
	advertsourceid: number | null;
	advertsource: {
		description: string | null;
		processid: number;
		communicationprocess: {
			description: string | null;
			rel: string | null;
			title: string | null;
			id: number;
			href: string | null;
		} | null;
		rel: string | null;
		title: string | null;
		id: number;
		href: string | null;
	} | null;
	active: boolean | null;
	optoutemarketing: boolean | null;
	salespersonid: number | null;
	salesperson: {
		name: string | null;
		jobtitle: string | null;
		rel: string | null;
		title: string | null;
		id: number;
		href: string | null;
	} | null;
	defaultcompany: {
		companytype: number;
		accountname: string;
		email: string | null;
		phone: string | null;
		postaladdress: {
			line1: string | null;
			line2: string | null;
			line3: string | null;
			line4: string | null;
			line5: string | null;
		};
		postalcode: string | null;
		deliveryaddress: {
			line1: string | null;
			line2: string | null;
			line3: string | null;
			line4: string | null;
			line5: string | null;
			line6: string | null;
		};
		defaultcontactid: number | null;
		defaultcontact: undefined;
		contacts: {
			rel: string | null;
			title: string | null;
			href: string | null;
		} | null;
		website: string | null;
		salespersonid: number | null;
		salesperson: {
			name: string | null;
			jobtitle: string | null;
			rel: string | null;
			title: string | null;
			id: number;
			href: string | null;
		} | null;
		balance: number;
		contactname: string | null;
		latitude: number | null;
		longitude: number | null;
		geocodestatus: number | null;
		extrafields: [] | null;
		rel: string | null;
		title: string | null;
		id: number;
		href: string | null;
	} | null;
	defaultcompanyid: { companytype: number; accno: number } | null;
	defaultcompanyname: string | null;
	latitude: number | null;
	longitude: number | null;
	geocodestatus: number | null;
	extrafields: [] | null;
	rel: string | null;
	title: string | null;
	id: number;
	href: string | null;

	hubspotid?: string | null;
}

// export interface HubspotContact {
// 	company: string | null;
// 	createdate?: string | null;
// 	email: string | null;
// 	firstname: string | null;
// 	lastmodifieddate?: string | null;
// 	lastname: string | null;
// 	phone: string | null;
// 	website?: string | null;
// 	jobtitle: string | null;
// }

// export interface BatchCreateUpdateHubspotContact {
// 	vid?: string | null;
// 	email?: string | null;
// 	properties: HubspotContactProperty[];
// }

// export interface HubspotContactProperty {
// 	property: string;
// 	value: string | null | undefined;
// }

export interface GetHubspotContacts {
	results: GetHubspotContact[];
}

export interface GetHubspotContact {
	id: string;
	properties: HubspotProperties;
	createdAt: string;
	updatedAt: string;
	archived: boolean;
}

export interface CreateHubspotContacts {
	inputs: CreateHubspotContact[];
}

export interface CreateHubspotContact {
	properties: HubspotProperties;
}

export interface UpdateHubspotContacts {
	inputs: UpdateHubspotContact[];
}

export interface UpdateHubspotContact {
	id?: string | null;
	properties: HubspotProperties;
}

export interface HubspotProperties {
	company: string | null;
	email: string | null;
	firstname: string | null;
	lastname: string | null;
	phone: string | null;
	website?: string | null;
	jobtitle: string | null;
	exo_id?: number | null;
}
