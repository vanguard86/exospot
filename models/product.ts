export interface ExoStockItem {
	id: string | null /* sku & name */;
	description: string | null /* description */;
	latestcost: number | null /* Unit Cost */;
	saleprices: [
		{
			name: string | null /* This is always "Base Price" */;
			price: number | null /* Unit Price */;
		}
	];
}

export interface CreateHubspotProduct {
	properties: HubspotProductProperties;
}

export interface HubspotProductProperties {
	description: string | null;
	hs_cost_of_goods_sold: string | null;
	hs_sku: string | null;
	name: string | null;
	price: string | null;
}
