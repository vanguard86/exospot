import { HttpClient } from "./http-client";
import { AxiosRequestConfig, AxiosResponse } from "axios";
import { ExoContact } from "../models/contact";
import { ExoStockItem } from "../models/product";

export class ExoApi extends HttpClient {
	public constructor() {
		super("https://exo.api.myob.com");

		this._initializeRequestInterceptor();
	}

	private _initializeRequestInterceptor = () => {
		this.instance.interceptors.request.use(
			this._handleRequest,
			this._handleError
		);
	};

	private _handleRequest = (config: AxiosRequestConfig) => {
		config.headers["x-myobapi-key"] = process.env.MYOBAPI_KEY;
		config.headers["x-myobapi-exotoken"] = process.env.MYOBAPI_EXOTOKEN;
		config.headers["Accept"] = "application/json";
		config.headers["Authorization"] = process.env.AUTHORIZATION;

		return config;
	};

	public getContacts = (page: number): Promise<AxiosResponse<ExoContact[]>> =>
		this.instance.get(`/contact`, {
			params: {
				pagesize: 100,
				page: page,
				$filter: `c.last_updated ge '${this.formatDate()}'`,
			},
		});

	public getStockItems = (page: number): Promise<AxiosResponse<ExoStockItem[]>> =>
		this.instance.get(`/stockitem/search?q=`, {
			params: {
				pagesize: 100,
				page: page,
				$filter: `s.last_updated ge '${this.formatDate()}'`,
			}
		})

	private formatDate() {
		let d = new Date(),
			month = '' + (d.getMonth() + 1),
			day = '' + (d.getDate() - 1),
			year = d.getFullYear();
	
		if (month.length < 2) 
			month = '0' + month;
		if (day.length < 2) 
			day = '0' + day;
	
		return [year, month, day].join('-');
	}
}
