import express from "express";
import { ExoApi } from "./apis/exo-api";
import { HubspotApi } from "./apis/hubspot-api";
import {
	ExoContact,
	GetHubspotContacts,
	GetHubspotContact,
	CreateHubspotContact,
	UpdateHubspotContact,
} from "./models/contact";

const app = express();
const PORT = 8080;
app.get("/", (req, res) => res.send("MYOB EXO + HubSpot Integration"));
app.listen(PORT, async (): Promise<void> => {
	console.log(`⚡️[EXOSpot]: Server is running at https://localhost:${PORT}`);

	try {
		// Get the APIs
		const exoApi = new ExoApi();
		const hubspotApi = new HubspotApi();

		// Initialise the loop variables
		let fetchingHubspotContacts: boolean = true;
		let hubspotContacts: GetHubspotContact[] = [];
		let hubspotLastId: number | null = null;
		let hubspotRes: GetHubspotContacts;

		// Retrieve the existing contacts from HubSpot
		while (fetchingHubspotContacts) {
			// Determine if a last id is needed to be used
			if (hubspotLastId) {
				hubspotRes = await hubspotApi.getContacts(hubspotLastId);
			} else {
				hubspotRes = await hubspotApi.getContacts();
			}

			if (hubspotRes.results.length) {
				hubspotContacts = [...hubspotContacts, ...hubspotRes.results];
				console.log(
					`• [HubSpot]: fetched ${hubspotContacts.length} contact(s)...`
				);
				hubspotLastId = +hubspotContacts[hubspotContacts.length - 1].id;
				if (hubspotRes.results.length != 100) {
					fetchingHubspotContacts = false;
				}
			} else {
				fetchingHubspotContacts = false;
			}
		}

		// Confirm Contacts
		console.log(`✓ [EXOSpot]: saved ${hubspotContacts.length} contact(s)`);

		// Initialise the loop variables
		let fetchingExoContacts: boolean = true;
		let exoContacts: ExoContact[] = [];
		let exoContactPage: number = 1;
		let exoRes: ExoContact[];

		while (fetchingExoContacts) {
			exoRes = await exoApi.getContacts(exoContactPage);

			if (exoRes.length) {
				exoContacts = [...exoContacts, ...exoRes];
				console.log(
					`• [MYOB EXO]: fetched ${exoContacts.length} contact(s)...`
				);
				exoContactPage = exoContactPage += 1;
				if (exoRes.length != 100) {
					fetchingExoContacts = false;
				}
			} else {
				fetchingExoContacts = false;
			}
		}

		// Confirm Contacts
		console.log(`✓ [EXOSpot]: saved ${exoContacts.length} contact(s)`);

		// Validate messy contacts
		exoContacts.forEach((contact) => {
			const re =
				/(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
			if (contact.email) {
				let extractedEmail = re.exec(contact.email);
				if (extractedEmail?.length) {
					contact.email = extractedEmail[0];
				} else {
					contact.email = null;
				}
			} else {
				contact.email = null;
			}
		});

		// Find contacts to update
		const contactsToUpdate = exoContacts
			.filter((exoContact) => {
				// If there's a contact with a matching EXO ID
				if (
					hubspotContacts
						.map((hubspotContact) =>
							hubspotContact.properties.exo_id
								? +hubspotContact.properties.exo_id
								: null
						)
						.filter((id) => id != null)
						.includes(exoContact.id)
				) {
					exoContact.hubspotid = hubspotContacts.find(
						(hubspotContact) =>
							hubspotContact.properties.exo_id == exoContact.id
					)?.id;
					return true;
				}
				if (exoContact.email != null) {
					console.log(
						hubspotContacts.map(
							(hubspotContact) => hubspotContact.properties.email
						)
					);
					if (
						hubspotContacts
							.map(
								(hubspotContact) =>
									hubspotContact.properties.email
							)
							.includes(exoContact.email)
					) {
						exoContact.hubspotid = hubspotContacts.find(
							(hubspotContact) =>
								hubspotContact.properties.email ==
								exoContact.email
						)?.id;
						return true;
					}
				}
				return false;
			})
			.map((contact): UpdateHubspotContact => {
				return {
					id: contact.hubspotid,
					properties: {
						company: contact.defaultcompanyname,
						email: contact.email,
						firstname: contact.firstname,
						lastname: contact.lastname,
						phone: contact.directphonenumber,
						website: contact.defaultcompany?.website,
						jobtitle: contact.jobtitle,
						exo_id: contact.id,
					},
				};
			});

		// Using the generated List
		let updatingContacts: boolean = true;
		let updateSliceStart: number = 0;
		let updateSliceEnd: number = 10;
		let updateCount: number = 0;

		// Update Contacts
		while (updatingContacts) {
			let contacts = contactsToUpdate.slice(
				updateSliceStart,
				updateSliceEnd
			);

			// Remove duplicate contacts
			contacts = contacts.filter((contact, index) => {
				return contacts.map((e) => e.id).indexOf(contact.id) === index;
			});

			if (contacts.length == 0) {
				updatingContacts = false;
			} else {
				await hubspotApi.updateContacts({
					inputs: contacts,
				});
				updateCount += contacts.length;
				console.log(
					`• [HubSpot]: Updating ${updateCount} contact(s)...`
				);
				updateSliceStart += 10;
				updateSliceEnd += 10;
			}
		}

		console.log(`✓ [EXOSpot]: Updated ${updateCount} contact(s)`);

		// Find Contacts to create
		const contactsToCreate = exoContacts
			.filter((exoContact) => {
				if (
					hubspotContacts
						.map((hubspotContact) =>
							hubspotContact.properties.exo_id
								? +hubspotContact.properties.exo_id
								: null
						)
						.filter((id) => id != null)
						.includes(exoContact.id)
				) {
					exoContact.hubspotid = hubspotContacts.find(
						(hubspotContact) =>
							hubspotContact.properties.exo_id == exoContact.id
					)?.id;
					return false;
				}
				if (exoContact.email != null) {
					if (
						hubspotContacts
							.map(
								(hubspotContact) =>
									hubspotContact.properties.email
							)
							.includes(exoContact.email)
					) {
						exoContact.hubspotid = hubspotContacts.find(
							(hubspotContact) =>
								hubspotContact.properties.email ==
								exoContact.email
						)?.id;
						return false;
					}
				}
				return true;
			})
			.map((contact): CreateHubspotContact => {
				return {
					properties: {
						company: contact.defaultcompanyname,
						email: contact.email,
						firstname: contact.firstname,
						lastname: contact.lastname,
						phone: contact.directphonenumber,
						website: contact.defaultcompany?.website,
						jobtitle: contact.jobtitle,
						exo_id: contact.id,
					},
				};
			});

		// Using the generated List
		let creatingContacts: boolean = true;
		let createSliceStart: number = 0;
		let createSliceEnd: number = 1;
		let createCount: number = 0;

		// Create contacts
		while (creatingContacts) {
			let contacts = contactsToCreate.slice(
				createSliceStart,
				createSliceEnd
			);
			if (contacts.length == 0) {
				creatingContacts = false;
			} else if (contacts.length != 1) {
				await hubspotApi
					.createContacts({
						inputs: contacts,
					})
					.catch(() => {
						hubspotApi.updateContacts({
							inputs: contacts,
						});
					});
				createCount += contacts.length;
				console.log(
					`• [HubSpot]: Creating ${createCount} contact(s)...`
				);
				creatingContacts = false;
			} else {
				await hubspotApi
					.createContacts({
						inputs: contacts,
					})
					.then(() => {
						createCount += contacts.length;
						console.log(
							`• [HubSpot]: Creating ${createCount} contact(s)...`
						);
					})
					.catch(() => {
						console.log("• [HubSpot] Skipping duplicate contact");
					});
				createSliceStart += 1;
				createSliceEnd += 1;
			}
		}

		console.log(`✓ [EXOSpot]: Created ${createCount} contact(s)`);
	} catch (error) {
		console.error(error);
	}

	/*
	exoApi.getContacts().then(async (contacts) => {
		// const contactList: CreateHubspotContact[] = (await contacts).map(
		// 	(contact): CreateHubspotContact => {
		// 		return {
		// 			properties: {
		// 				company: contact.defaultcompanyname,
		// 				email: contact.email,
		// 				firstname: contact.firstname,
		// 				lastname: contact.lastname,
		// 				phone: contact.directphonenumber,
		// 				website: contact.defaultcompany?.website,
		// 				jobtitle: contact.jobtitle
		// 			}
		// 		};
		// 	}
		// );

		// const batchContacts: BatchCreateHubspotContact = {
		// 	inputs: contactList
		// }
		console.log(`✓ [exo]: Pulled ${(await contacts).length} contact(s)`);
		const batchContacts: BatchCreateUpdateHubspotContact[] = (await contacts).map(
			(contact): BatchCreateUpdateHubspotContact => {
				return {
					// email: contact.email,
					properties: [
						{ property: 'company', value: contact.defaultcompanyname },
						{ property: 'email', value: contact.email },
						{ property: 'firstname', value: contact.firstname },
						{ property: 'lastname', value: contact.lastname },
						{ property: 'phone', value: contact.directphonenumber },
						{ property: 'website', value: contact.defaultcompany?.website },
						{ property: 'jobtitle', value: contact.jobtitle }
					].filter(property => {
						return property.value
					})
				}
			}
		)

		hubspotApi.createUpdateContactBulk(batchContacts).then(() => {
			console.log(`✓ [hubspot]: Created and updated ${batchContacts.length} contact(s)`);
		}).catch((error) => {
			console.log(error);
		})

		// contactList.forEach(contact => {
		// 	hubspotApi.createContact(contact)
		// 	.then(() => {
				
		// 	})
		// 	.catch((error) => {
		// 		console.log("✗ [hubspot]: Contact already exists")
		// 	})
		// });
	});
	*/
});

function delay(ms: number) {
	return new Promise((resolve) => setTimeout(resolve, ms));
}
