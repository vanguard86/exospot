import { HttpClient } from "./http-client";
import { AxiosRequestConfig, AxiosResponse } from "axios";
import {
	GetHubspotContacts,
	CreateHubspotContacts,
	UpdateHubspotContacts
} from "../models/contact";
import { CreateHubspotProduct } from "../models/product";

export class HubspotApi extends HttpClient {
	public constructor() {
		super("https://api.hubapi.com", {
			hapikey: process.env.HAPIKEY,
		});

		this._initializeRequestInterceptor();
	}

	private _initializeRequestInterceptor = () => {
		this.instance.interceptors.request.use(
			this._handleRequest,
			this._handleError
		);
	};

	private _handleRequest = (config: AxiosRequestConfig) => {
		config.headers["Accept"] = "application/json";
		config.headers["content-type"] = "application/json";

		return config;
	};

	public getContacts = (lastId?: number): Promise<AxiosResponse<GetHubspotContacts>> =>
		this.instance.get(`/crm/v3/objects/contacts`, {
			params: { limit: 100, after: lastId, properties: 'email,exo_id' },
		});

	// public createContact = (
	// 	body: CreateHubspotContact
	// ): Promise<AxiosResponse<any>> =>
	// 	this.instance.post("/crm/v3/objects/contacts", body);

	public createContacts = (
		body: CreateHubspotContacts
	): Promise<AxiosResponse<any>> =>
		this.instance.post("/crm/v3/objects/contacts/batch/create", body);

	// public createUpdateContactBulk = (
	// 	body: BatchCreateUpdateHubspotContact[]
	// ): Promise<AxiosResponse<any>> =>
	// 	this.instance.post("/contacts/v1/contact/batch", body);

	public updateContacts = (
		body: UpdateHubspotContacts
	): Promise<AxiosResponse<any>> =>
		this.instance.post("/crm/v3/objects/contacts/batch/update", body);

	public createProduct = (
		body: CreateHubspotProduct
	): Promise<AxiosResponse<any>> =>
		this.instance.post("/crm/v3/objects/products/", body);
}
